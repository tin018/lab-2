/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author tin018
 *
 */
public class Celsius extends Temperature
{	
  public Celsius(float t)
  {
    super(t);
  }
  public String toString()
  {
  return this.getValue() + " C";
  }
@Override
public Temperature toCelsius() {
    return new Celsius(this.getValue());
}
@Override
public Temperature toFahrenheit() {
	return new Fahrenheit(this.getValue()*(float)(9)/5 +32);
}
}