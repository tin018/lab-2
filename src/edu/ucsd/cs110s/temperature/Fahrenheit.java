/**
 * 
 */
package edu.ucsd.cs110s.temperature;

/**
 * @author tin018
 *
 */
public class Fahrenheit extends Temperature
{
  public Fahrenheit(float t)
  {
    super(t);
  }
  
  public String toString()
  {
    return this.getValue() + " F";
  }

@Override
public Temperature toCelsius() {
	return new Celsius((float)(this.getValue()-32)*(float)(5)/9);
}

@Override
public Temperature toFahrenheit() {
	return new Fahrenheit((this.getValue()));
}
}